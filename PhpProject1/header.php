<?php
include './database.php';
session_start();
if (isset($_SESSION["user_id"])) {
    $id = $_SESSION["user_id"];
    $result = query("select * from users where users_id=$id");
    $data = mysqli_fetch_row($result);
    if ($data > 0) {
        $username = $data["1"];
    }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="pull-right">


                <?php if ($_SESSION["user"] == "") { ?>
                    <span><a href="login.php" class="btn btn-default">Login</a></span>
                    <span><a href="register.php" class="btn btn-default">Register</a></span>
                <?php } ?>
                <?php if ($_SESSION["user"] == "general" || $_SESSION["user"] == "admin") { ?>
                    <span><a href="user.php" class="btn btn-default">User : <?php echo $username; ?></a></span>
                    <span><a href="logout.php" class="btn btn-default">Exit</a></span>
                <?php } ?>
                <span><a href="index.php" class="btn btn-default">Home</a></span>
            </div>
            <?php
            if ($_SESSION["user"] != "admin") {
                ?>
                <div class="pull-left"><a href="<?php
                    if ($_SESSION["user"] == "general") {
                        echo "shop-card.php";
                    } else {
                        echo "login.php";
                    }
                    ?>" class="btn btn-default">Shopping Card</a></div>
                <?php } ?>
        </div>