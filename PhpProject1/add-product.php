<?php
include './header.php';
if ($_SESSION["user"] != "admin")
    header("location:admin-login.php");
?>

<form class="form-horizontal" method="post" action="add-product-confirm.php" enctype="multipart/form-data">
    <fieldset>

        <!-- Form Name -->
        <legend>Add</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <input id="name" name="name" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="price">Price</label>
            <div class="col-md-4">
                <input id="price" name="price" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="description">Description</label>
            <div class="col-md-4">
                <input id="description" name="description" placeholder="" class="form-control input-md">

            </div>
        </div>



        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="quantity">Quantity</label>
            <div class="col-md-4">
                <input id="quantity" name="quantity" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="photo">Photo</label>
            <div class="col-md-4">
                <input type="file" class="form-control" id="photo" name="photo"/>
            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="register"></label>
            <div class="col-md-8">
                <button id="register" name="edit" class="btn btn-success" type="submit">Add</button>
                <button id="clear" name="clear" class="btn btn-danger" type="reset">Clear</button>
            </div>
        </div>

    </fieldset>
</form>
<?php
include './footer.php';
?>