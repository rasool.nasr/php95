<?php
include './header.php';
if ($_SESSION["user"] != "general")
    header("location:login.php");
if (isset($_GET['id'])) {
    array_push($_SESSION[card], $_GET['id']);
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Photo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total = 0;
                $result = $_SESSION["card"];
                foreach ($result as $data) {
                    $result_set = query("select * from product where product_id=$data");
                    $result_set = mysqli_fetch_row($result_set);
                    $total = $total + $result_set['2'];
                    echo '<tr class=center>';
                    echo "<td>" . $result_set['0'] . "</td>";
                    echo "<td>" . $result_set['1'] . "</td>";
                    echo "<td>" . $result_set['2'] . "</td>";
                    echo "<td>" . $result_set['3'] . "</td>";
                    echo "<td>" . $result_set['5'] . "</td>";
                    echo '<td><img src="uploads/' . $result_set['4'] . '" width=100px height=100px/></td>';
                    echo '<tr>';
                }
                ?>



            </tbody>
        </table>
        <span>
            <a href="confirm-buy.php" class="btn btn-success">Buy</a>
            Total:<?php echo $total; ?>
        </span>
    </body>
</html>
