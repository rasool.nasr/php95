<?php
include './header.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="container">

            <img src="img/slide3.jpg" class="img-responsive"/>
            <div class="row">
                <?php
                $result = query("select * from product");
                foreach ($result as $data) {
                    ?>

                    <div class="col-md-3 col-xs-12 center img-thumbnail">
                        <img src="uploads/<?php echo $data['photo']; ?>" width="200px" height="200px"/>
                        <p><?php echo $data['name'] ?></p>
                        <span>
                            <a href="product-detail.php?id=<?php echo $data['product_id']; ?>">
                                <span class="glyphicon glyphicon-search"></span>

                            </a>
                            <a href="shop-card.php?id=<?php echo $data['product_id']; ?>">
                                <span class="glyphicon glyphicon-plus"></span>

                            </a>
                        </span>
                    </div>


                    <?php
                }
                ?>
            </div>
        </div>
    </body>
</html>
