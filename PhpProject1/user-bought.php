<?php
include './header.php';
if ($_SESSION["user"] != "general")
    header("location:login.php");
if ($_GET["buy"] === "buy") {
    $confirm = true;
}
//echo var_dump($_SESSION[card]);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <br>
        <br>
        <br>

        <?php
        if ($confirm == true)
            echo '<span class="alert alert-success">Your Shopping was finished successfully</span>';
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Photo</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $id = $_SESSION["user_id"];
                $result = query("SELECT product.product_id,product.`name`,product.price,product.description,orders.`date`,product.photo FROM product join orders on orders.products_id=product.product_id where orders.user_id=$id;");
                foreach ($result as $data) {
                    echo '<tr class=center>';
                    echo "<td>" . $data['product_id'] . "</td>";
                    echo "<td>" . $data['name'] . "</td>";
                    echo "<td>" . $data['price'] . "</td>";
                    echo "<td>" . $data['description'] . "</td>";
                    echo "<td>" . $data['date'] . "</td>";
                    echo '<td><img src="uploads/' . $data['photo'] . '" width=100px height=100px/></td>';
                    echo '<tr>';
                }
                ?>


            </tbody>
        </table>
    </body>
</html>
