<?php
include './header.php';
if ($_SESSION["user"] != "admin")
    header("location:admin-login.php");
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $result = query("select * from users where users_id=$id");
    $data = mysqli_fetch_row($result);
    if ($data > 0)
        $name = $data["3"];
    $username = $data["1"];
    $password = $data["2"];
    $email = $data["6"];
    $phone = $data["5"];
    $address = $data["4"];
//    echo print_r($data);
}
if (count($_POST) > 0) {
    $id_edit = $_POST["id"];
    $name_edit = $_POST["name"];
    $username_edit = $_POST["username"];
    $password_edit = $_POST["password"];
    $email_edit = $_POST["email"];
    $phone_edit = $_POST["phone"];
    $address_edit = $_POST["address"];
    $result = query("UPDATE `users` SET `username` = '$username_edit', `password` = '$password_edit', `name` = '$name_edit', `address` = '$address_edit', `phone` = '$phone_edit', `email` = '$email_edit' WHERE `users`.`users_id` = $id_edit");
    if ($result)
        header("location:user-list.php");
}
include './header.php';
?>

<form class="form-horizontal" method="post" action="">
    <fieldset>

        <!-- Form Name -->
        <legend>Edit</legend>
        <input type="hidden" name="id" value="<?php echo $id; ?>">

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <input id="name" name="name" type="text" placeholder="" class="form-control input-md" value="<?php echo $name; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="username">Username</label>
            <div class="col-md-4">
                <input id="username" name="username" type="text" placeholder="" class="form-control input-md" value="<?php echo $username; ?>">

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <input id="password" name="password" type="password" placeholder="" class="form-control input-md" value="<?php echo $password; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                <input id="email" name="email" type="text" placeholder="" class="form-control input-md" value="<?php echo $email; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="phone">Phone</label>
            <div class="col-md-4">
                <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md" value="<?php echo $phone; ?>">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="address">Address</label>
            <div class="col-md-4">
                <textarea class="form-control" id="address" name="address" value="<?php echo $address; ?>"></textarea>
            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="register"></label>
            <div class="col-md-8">
                <button id="register" name="edit" class="btn btn-success" type="submit">Edit</button>
                <button id="clear" name="clear" class="btn btn-danger" type="reset">Clear</button>
            </div>
        </div>

    </fieldset>
</form>
<?php
include './footer.php';
?>