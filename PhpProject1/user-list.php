
<?php
include './header.php';
include './admin-menu.php';
if ($_SESSION["user"] != "admin")
    header("location:admin-login.php");
?>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Username</th>
            <th>Password</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $result = query("select * from users");
        foreach ($result as $data) {
            echo '<tr>';
            echo "<td>" . $data['users_id'] . "</td>";
            echo "<td>" . $data['name'] . "</td>";
            echo "<td>" . $data['username'] . "</td>";
            echo "<td>" . $data['password'] . "</td>";
            echo "<td>" . $data['email'] . "</td>";
            echo "<td>" . $data['phone'] . "</td>";
            echo "<td>" . $data['address'] . "</td>";
            echo '<td><a href="user-delete.php?id=' . $data["users_id"] . '"><span class="glyphicon glyphicon-remove"></span></a></td>';
            echo '<td><a href="edit-user.php?id=' . $data["users_id"] . '"><span class="glyphicon glyphicon-log-out"></span></a></td>';
            echo '<tr>';
        }
        ?>


    </tbody>
</table>


<?php
include './footer.php';
?>