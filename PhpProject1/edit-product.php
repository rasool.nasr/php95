<?php
include './header.php';
if ($_SESSION["user"] != "admin")
    header("location:admin-login.php");
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $result = query("select * from product where product_id=$id");
    $data = mysqli_fetch_row($result);
    if ($data > 0)
        $name = $data["1"];
    $price = $data["2"];
    $description = $data["3"];
    $date = $data["5"];
    $quantity = $data["6"];
    $photo = $data["4"];
//    echo print_r($data);
}
if (count($_POST) > 0) {
    $id_edit = $_POST["id"];
    $name_edit = $_POST["name"];
    $price_edit = $_POST["price"];
    $description_edit = $_POST["description"];
    $quantity_edit = $_POST["quantity"];
    $photo_edit = $_POST["photo"];
    $result = query("UPDATE `product` SET `name` = '$name_edit', `price` = '$price_edit', `description` = '$description_edit', `quantity` = '$quantity_edit', `photo` = '$photo_edit' WHERE `product`.`product_id` = $id_edit");
    if ($result)
        header("location:list-product.php");
}
include './header.php';
?>

<form class="form-horizontal" method="post" action="">
    <fieldset>

        <!-- Form Name -->
        <legend>Edit</legend>
        <input type="hidden" name="id" value="<?php echo $id; ?>">

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <input id="name" name="name" type="text" placeholder="" class="form-control input-md" value="<?php echo $name; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="price">Price</label>
            <div class="col-md-4">
                <input id="price" name="price" type="text" placeholder="" class="form-control input-md" value="<?php echo $price; ?>">

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="description">Description</label>
            <div class="col-md-4">
                <input id="description" name="description" placeholder="" class="form-control input-md" value="<?php echo $description; ?>">

            </div>
        </div>


        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="quantity">Quantity</label>
            <div class="col-md-4">
                <input id="quantity" name="quantity" type="text" placeholder="" class="form-control input-md" value="<?php echo $quantity; ?>">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="photo">Photo</label>
            <div class="col-md-4">
                <textarea class="form-control" id="photo" name="photo" value="<?php echo $photo; ?>"></textarea>
            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="register"></label>
            <div class="col-md-8">
                <button id="register" name="edit" class="btn btn-success" type="submit">Edit</button>
                <button id="clear" name="clear" class="btn btn-danger" type="reset">Clear</button>
            </div>
        </div>

    </fieldset>
</form>
<?php
include './footer.php';
?>