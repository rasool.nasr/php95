<?php
include './header.php';
if (count($_POST) > 0) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $result = query("select * from admin where username='$username' and password='$password'");
//    print_r(mysqli_fetch_array($result));
    if (count(mysqli_fetch_array($result)) > 0) {
        $_SESSION["user_id"] = $userarray[0];
        $_SESSION["user"] = "admin";
        header("location:admin.php");
    }
}
?>

<form class="form-horizontal" method="post" action="">
    <fieldset>

        <!-- Form Name -->
        <legend>Login</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="username">Username</label>
            <div class="col-md-4">
                <input id="username" name="username" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <input id="password" name="password" type="password" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="register"></label>
            <div class="col-md-8">
                <button id="register" name="login" class="btn btn-success" type="submit">Login</button>
                <button id="clear" name="clear" class="btn btn-danger" type="reset">Clear</button>
            </div>
        </div>
        <?php
        if (isset($_POST["login"]))
            echo '<div class="alert alert-danger" role="alert">Username or password is incorrect</div>';
        ?>
    </fieldset>

</form>
<?php
include './footer.php';
?>