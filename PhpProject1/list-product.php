
<?php
include './header.php';
include './admin-menu.php';
if ($_SESSION["user"] != "admin")
    header("location:admin-login.php");
?>
<a href="add-product.php" class="btn btn-default" style="margin: 5px">Add</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Date</th>
            <th>Quantity</th>
            <th>Photo</th>

        </tr>
    </thead>
    <tbody>
        <?php
        $result = query("select * from product");
        foreach ($result as $data) {
            echo '<tr class=center>';
            echo "<td>" . $data['product_id'] . "</td>";
            echo "<td>" . $data['name'] . "</td>";
            echo "<td>" . $data['price'] . "</td>";
            echo "<td>" . $data['description'] . "</td>";
            echo "<td>" . $data['date'] . "</td>";
            echo "<td>" . $data['quantity'] . "</td>";
            echo '<td><img src="uploads/' . $data['photo'] . '" width=100px height=100px/></td>';
            echo '<td><a href="delete-product.php?id=' . $data["product_id"] . '"><span class="glyphicon glyphicon-remove"></span></a></td>';
            echo '<td><a href="edit-product.php?id=' . $data["product_id"] . '"><span class="glyphicon glyphicon-log-out"></span></a></td>';
            echo '<tr>';
        }
        ?>


    </tbody>
</table>



<?php
include './footer.php';
?>