<?php
include './header.php';
if ($_SESSION["user"] != "general")
    header("location:login.php");
$id = $_SESSION["user_id"];
$result = query("select * from users where users_id=$id");
$data = mysqli_fetch_row($result);
if ($data > 0) {
    $name = $data["3"];
    $username = $data["1"];
    $password = $data["2"];
    $email = $data["6"];
    $phone = $data["5"];
    $address = $data["4"];
//    echo print_r($data);
}
?>

<form class="form-horizontal">
    <fieldset>
        <a href="user-bought.php" class="btn btn-default">Bought List</a>
        <!-- Form Name -->
        <legend>Edit</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <?php echo $name; ?>

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="username">Username</label>
            <div class="col-md-4">
                <?php echo $username; ?>

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <?php echo $password; ?>

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                <?php echo $email; ?>

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="phone">Phone</label>
            <div class="col-md-4">
                <?php echo $phone; ?>

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="address">Address</label>
            <div class="col-md-4">
                <?php echo $address; ?>
            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="register"></label>
            <div class="col-md-8">
                <a href="user_edit.php" class="btn btn-default">Edit</button>
            </div>
        </div>

    </fieldset>
</form>
<?php
include './footer.php';
?>